import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

interface EventAttr {
  calendarId: string
  groupId?: string
  allDay?: boolean
  location?: string
  start: string
  end?: string
  title: string
  url?: string
  classNames?: [string]
  editable?: boolean
  startEditable?: boolean
  durationEditable?: boolean
  resourceEditable?: boolean
  display?:
    | 'auto'
    | 'block'
    | 'list-item'
    | 'background'
    | 'inverse-background'
    | 'none'
  overlap?: boolean
  constraint?: object
  backgroundColor?: string
  borderColor?: string
  textColor?: string
  extendedProps?: object
  source?: object
}

// This Event Model at it's core is the fullcalendar event object
// https://fullcalendar.io/docs/event-object
interface EventDoc extends mongoose.Document {
  version: number
  calendarId: string
  groupId: string
  allDay: boolean
  start: string
  end: string
  title: string
  url: string
  classNames: [string]
  editable: boolean
  startEditable: boolean
  durationEditable: boolean
  resourceEditable: boolean
  display:
    | 'auto'
    | 'block'
    | 'list-item'
    | 'background'
    | 'inverse-background'
    | 'none'
  overlap: boolean
  constraint: object
  backgroundColor: string
  borderColor: string
  textColor: string
  extendedProps: object
  source: object
}

interface EventModel extends mongoose.Model<EventDoc> {
  build(attrs: EventAttr): EventDoc
  findByGroup(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<EventDoc | null>
}

const EventSchema = new mongoose.Schema(
  {
    calendarId: {
      type: mongoose.Types.ObjectId,
      ref: 'Calendar',
      required: true,
    },
    groupId: {
      type: String,
    },
    allDay: Boolean,
    start: { type: String, required: true },
    end: String,
    title: String,
    url: String,
    classNames: {
      type: [String],
    },
    editable: Boolean,
    backgroundColor: String,
    borderColor: String,
    textColor: String,
    extendedProps: Object,
    source: Object,
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

EventSchema.set('versionKey', 'version')
EventSchema.plugin(updateIfCurrentPlugin)

EventSchema.statics.findByGroup = (id, currentUser) => {
  return Event.findOne({
    _id: id,
    groupId: currentUser.groups[0],
  })
}
EventSchema.statics.build = (attrs: EventAttr) => {
  return new Event({
    ...attrs,
  })
}

const Event = mongoose.model<EventDoc, EventModel>('Event', EventSchema)

export { Event }
