import express from 'express'
import 'express-async-errors'
import { json } from 'body-parser'
import cookieSession from 'cookie-session'
import {
  currentUser,
  errorHandler,
  livenessRouter,
  NotFoundError,
} from '@life.stack/common'
import { newCalendarRoute } from './routes/calendars/newCalendar'
import { getCalendarRouter } from './routes/calendars/getCalendar'
import { newEventRouter } from './routes/events/newEvent'
import { getEventsRouter } from './routes/events/getEvents'
import { getCalendarsRouter } from './routes/calendars/getCalendars'
import { fullCalendarRouter } from './routes/events/fullCalendar'
import { updateEventRouter } from './routes/events/updateEvent'
import { removeEventRouter } from './routes/events/removeEvent'

const app = express()
app.set('trust proxy', true) // App is behind nginx proxy and should trust it
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
)
app.use(currentUser)

//  App Specific
// /api/calendars
app.use(newCalendarRoute)
app.use(getCalendarRouter)
app.use(getCalendarsRouter)
app.use(newEventRouter)
app.use(fullCalendarRouter)
app.use(getEventsRouter)
app.use(updateEventRouter)
app.use(removeEventRouter)

// Extra
app.use(livenessRouter)

// If app routes don't work, throw a not found
app.all('*', () => {
  throw new NotFoundError()
})

// Error Handler
app.use(errorHandler)

export { app }
