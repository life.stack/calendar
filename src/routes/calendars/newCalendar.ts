import express, { Request, Response } from 'express'
import {
  BadRequestError,
  requireAuth,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { Calendar } from '../../models/calendar'

const newCalendarRoute = express.Router()

newCalendarRoute.post(
  '/api/calendars',
  requireAuth,
  [body('name').trim().isString().isLength({ min: 1, max: 128 })],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name } = req.body
    const groupId = req.currentUser?.groups[0]

    const calendar = Calendar.build({
      name,
      groupId: groupId!,
      ownerId: req.currentUser!.id,
    })
    await calendar.save()

    res.status(201).send(calendar)
  }
)

export { newCalendarRoute }
