import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Calendar } from '../../models/calendar'

const getCalendarsRouter = express.Router()

getCalendarsRouter.get(
  '/api/calendars',
  requireAuth,
  async (req: Request, res: Response) => {
    const calendars = await Calendar.find({
      groupId: req.currentUser!.groups[0],
    })

    if (!calendars) {
      throw new NotFoundError()
    }

    res.status(200).send(calendars)
  }
)

export { getCalendarsRouter }
