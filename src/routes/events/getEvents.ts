import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Calendar } from '../../models/calendar'
import { Event } from '../../models/event'

const getEventsRouter = express.Router()

getEventsRouter.get(
  '/api/calendars/:calendarId/events',
  requireAuth,
  async (req: Request, res: Response) => {
    const { calendarId } = req.params

    const calendar = await Calendar.findByGroup(calendarId, req.currentUser!)

    if (!calendar) {
      throw new NotFoundError()
    }

    const events = await Event.find({ calendarId: calendarId })

    res.status(200).send(events)
  }
)

export { getEventsRouter }
