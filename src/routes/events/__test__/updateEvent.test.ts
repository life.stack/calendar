import request from 'supertest'
import { app } from '../../../app'
import { Event } from '../../../models/event'

const setup = async () => {
  const user = global.signin()

  const { body: createdCalendar } = await request(app)
    .post('/api/calendars')
    .set('Cookie', user)
    .send({ name: 'test cal' })

  const { body: createdEvent } = await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send({ title: 'update event', start: '2020-01-01' })

  return { user, createdCalendar, createdEvent }
}

it('has a route handler for PUT /api/calendars/:calendarId/events/:eventId', async () => {
  const { user, createdCalendar, createdEvent } = await setup()

  await request(app)
    .put(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', user)
    .send({ title: 'updated title' })
    .expect(200)

  await request(app)
    .put(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .send()
    .expect(401)
})

it('updates an event', async () => {
  const { user, createdCalendar, createdEvent } = await setup()

  await request(app)
    .put(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', user)
    .send({ title: 'updated event' })
    .expect(200)

  const eventInDb = await Event.findById(createdEvent.id)
  expect(eventInDb!.title).toEqual('updated event')
})

it('updates only relevant properties', async () => {
  const { user, createdCalendar, createdEvent } = await setup()

  await request(app)
    .put(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', user)
    .send({ title: 'updated event', notarealprop: 'bad data' })
    .expect(200)

  const eventInDb = await Event.findById(createdEvent.id)
  expect(eventInDb!.title).toEqual('updated event')
  // @ts-ignore
  expect(eventInDb!.notarealprop).toBeUndefined()
})
