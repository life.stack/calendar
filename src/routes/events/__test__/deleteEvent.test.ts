import request from 'supertest'
import { app } from '../../../app'
import { Event } from '../../../models/event'

const setup = async () => {
  const user = global.signin()

  const { body: createdCalendar } = await request(app)
    .post('/api/calendars')
    .set('Cookie', user)
    .send({ name: 'test cal' })

  const { body: createdEvent } = await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send({ title: 'update event', start: '2020-01-01' })

  return { user, createdCalendar, createdEvent }
}

it('has a route handler for DELETE /api/calendars/:calendarId/events/:eventId', async () => {
  const { user, createdCalendar, createdEvent } = await setup()

  const { body: firstDelete } = await request(app)
    .delete(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)
})

it('removes event from the database', async () => {
  const { user, createdCalendar, createdEvent } = await setup()

  const { body: firstDelete } = await request(app)
    .delete(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  const eventInDb = await Event.findById(firstDelete.id)
  expect(eventInDb).toEqual(null)
})

it('user can only delete their groups events', async () => {
  const { user, createdCalendar, createdEvent } = await setup()

  const { body: firstDelete } = await request(app)
    .delete(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', global.signin())
    .send()
    .expect(404)

  const eventInDb = await Event.findById(createdEvent.id)
  expect(eventInDb).not.toEqual(null)
  expect(eventInDb!.id).toEqual(createdEvent.id)
  expect(eventInDb!.title).toEqual(createdEvent.title)
  expect(eventInDb!.start).toEqual(createdEvent.start)

  const { body: secondDelete } = await request(app)
    .delete(`/api/calendars/${createdCalendar.id}/events/${createdEvent.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)
})
