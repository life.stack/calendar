import { app } from '../../../app'
import request from 'supertest'

const setup = async () => {
  const user = global.signin()

  const { body: createdCalendar } = await request(app)
    .post('/api/calendars')
    .set('Cookie', user)
    .send({ name: 'test cal' })

  return { user, createdCalendar }
}

it('has a route handler for POST /api/calendars/:calendarId/events', async () => {
  const { createdCalendar } = await setup()
  const response = await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .send()

  expect(response.statusCode).not.toEqual(404)
})

it('requires authorization', async () => {
  const { user, createdCalendar } = await setup()

  await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .send({ title: 'event', start: '2020-01-01' })
    .expect(401)

  await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .send({ title: 'event', start: '2020-01-01' })
    .set('Cookie', user)
    .expect(201)
})
