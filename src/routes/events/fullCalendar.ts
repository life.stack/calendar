import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Calendar } from '../../models/calendar'
import { Event } from '../../models/event'

const fullCalendarRouter = express.Router()

type ReqQuery = { start: string; end: string }
type ReqBody = {}

fullCalendarRouter.get(
  '/api/calendars/:calendarId/fullcalendar',
  requireAuth,
  async (req: Request<any, any, ReqBody, ReqQuery>, res: Response) => {
    const { calendarId } = req.params
    const { start, end }: { start: string; end: string } = req.query
    console.log('fullcalendar events ', start, end, req.body)

    const calendar = await Calendar.findByGroup(calendarId, req.currentUser!)

    if (!calendar) {
      throw new NotFoundError()
    }

    const events = await Event.find({
      calendarId: calendarId,
      start: { $gte: start, $lte: end },
    })

    res.status(200).send(events)
  }
)

export { fullCalendarRouter }
